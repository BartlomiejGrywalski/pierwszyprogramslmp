﻿using System;
using System.Net;
using System.Net.Sockets;

namespace PierwszyProgramSLMP
{
    class Program
    {
        static TcpClient tcpClient = new TcpClient();
        static void Main(string[] args)
        {
            IPAddress iPAddress = new IPAddress(new byte[] { 192, 168, 3, 250 });
            ConnectTCP(iPAddress, 2000);
            if (tcpClient.Connected)
            {
                Console.WriteLine("Połączono z PLC");
                ReadD200Return readD200Return = Read_D200();
                if (readD200Return.corectFinished)
                {
                    bool correcrtWriteD200 = Write_D200(++readD200Return.d200Value);
                    if (correcrtWriteD200)
                    {
                        Read_D200();
                    }
                }
            }
            else
            {
                Console.WriteLine("[Błąd] Nie można połączyć się z PLC");
            }
            tcpClient.Close();
            Console.ReadKey();
        }
        private static bool Write_D200(short d200Value)
        {
            bool correctWriteD200 = false;
            byte[] d200ValueInBytes = BitConverter.GetBytes(d200Value);
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0E, 0x00, 0x10, 0x00,
                0x01, 0x14, 0x00, 0x00, 0xC8, 0x00, 0x00, 0xA8, 0x01, 0x00, d200ValueInBytes[0], d200ValueInBytes[1]};
            NetworkStream networkStream = tcpClient.GetStream();
            networkStream.Write(payload, 0, payload.Length);
            networkStream.ReadTimeout = 1000;
            byte[] data = new byte[20];
            try
            {
                int numberOfReadBytes = networkStream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    correctWriteD200 = true;
                    Console.WriteLine("Poprawnie zapisano wartość D200");
                }
                else
                {
                    short errorCode = BitConverter.ToInt16(data, 9);
                    Console.WriteLine("[Błąd] Nie zapisano poprawnie D200, kod błędu: " + errorCode.ToString("X"));
                }
            }
            catch
            {
                Console.WriteLine("[Błąd] Utracono odpowiedź ze sterownika");
            }
            return correctWriteD200;
        }
        struct ReadD200Return
        {
            public bool corectFinished;
            public short d200Value;
        }
        private static ReadD200Return Read_D200()
        {
            ReadD200Return readD200Return = new ReadD200Return();
            readD200Return.corectFinished = false;
            byte[] payload = new byte[] { 0x50, 0x00, 0x00, 0xff, 0xff, 0x03, 0x00, 0x0C, 0x00, 0x10, 0x00,
                0x01, 0x04, 0x00, 0x00, 0xC8, 0x00, 0x00, 0xA8, 0x01, 0x00 };
            NetworkStream networkStream = tcpClient.GetStream();
            networkStream.Write(payload, 0, payload.Length);
            networkStream.ReadTimeout = 1000;
            byte[] data = new byte[20];
            try
            {
                int numberOfReadBytes = networkStream.Read(data, 0, data.Length);
                if (data[9] == 0 && data[10] == 0)
                {
                    readD200Return.d200Value = BitConverter.ToInt16(data, 11);
                    readD200Return.corectFinished = true;
                    Console.WriteLine("Odczytano poprawnie D200: " + readD200Return.d200Value);
                }
                else
                {
                    short errorCode = BitConverter.ToInt16(data, 9);
                    Console.WriteLine("[Błąd] Nie odczytano poprawnie D200, kod błędu: " + errorCode.ToString("X"));
                }
            }
            catch
            {
                Console.WriteLine("[Błąd] Utracono odpowiedź ze sterownika");
            }
            return readD200Return;
        }
        #region Part of code for generci TcpClinet perform connection for more info please check C# documnetation
        static void ConnectTCP(IPAddress IPAddressToConnect, int portNumber)
        {
            tcpClient.ReceiveTimeout = 5;
            tcpClient.SendTimeout = 5;
            try
            {
                tcpClient = Connect(IPAddressToConnect, portNumber, 1000);
            }
            catch
            {
                Console.WriteLine("Port Open FAIL");
            }
        }
        static TcpClient Connect(IPAddress hostName, int port, int timeout)
        {
            var client = new TcpClient();
            var state = new State { Client = client, Success = true };
            IAsyncResult ar = client.BeginConnect(hostName, port, EndConnect, state);
            state.Success = ar.AsyncWaitHandle.WaitOne(timeout, false);
            if (!state.Success || !client.Connected)
                throw new Exception("Failed to connect.");
            return client;
        }
        private class State
        {
            public TcpClient Client { get; set; }
            public bool Success { get; set; }
        }
        static void EndConnect(IAsyncResult ar)
        {
            var state = (State)ar.AsyncState;
            TcpClient client = state.Client;
            try
            {
                client.EndConnect(ar);
            }
            catch { }
            if (client.Connected && state.Success)
                return;
            client.Close();
        }
        #endregion
    }
}